<?php

namespace Drupal\simple_live_css\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Asset\AssetCollectionOptimizerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\simple_live_css\Utility\InjectCssFileUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LiveCssController.
 *
 * @package Drupal\simple_live_css\Controller
 */
class LiveCssController extends ControllerBase {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The CSS collection optimizer.
   *
   * @var \Drupal\Core\Asset\AssetCollectionOptimizerInterface
   */
  protected $cssCollectionOptimizerLazy;

  /**
   * The JS collection optimizer.
   *
   * @var \Drupal\Core\Asset\AssetCollectionOptimizerInterface
   */
  protected $jsCollectionOptimizerLazy;

  /**
   * Constructs a ConfigController object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Asset\AssetCollectionOptimizerInterface $css_collection_optimizer
   *   The CSS collection optimizer.
   * @param \Drupal\Core\Asset\AssetCollectionOptimizerInterface $js_collection_optimizer
   *   The JS collection optimizer.
   */
  public function __construct(FileSystemInterface $file_system, AssetCollectionOptimizerInterface $css_collection_optimizer, AssetCollectionOptimizerInterface $js_collection_optimizer) {
    $this->fileSystem = $file_system;
    $this->cssCollectionOptimizerLazy = $css_collection_optimizer;
    $this->jsCollectionOptimizerLazy = $js_collection_optimizer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('asset.css.collection_optimizer'),
      $container->get('asset.js.collection_optimizer')
    );
  }

  /**
   * Save the user entered live css.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An HTTP response.
   */
  public function save(Request $request): Response {
    $css_code = $request->request->get('css');
    $admin = $request->request->get('admin');

    $css_path = InjectCssFileUtility::FILE_PATH;
    if ($admin) {
      $css_path = InjectCssFileUtility::FILE_PATH_ADMIN;
    }

    // If the file didn't exist yet, clear all the caches. This will make
    // sure the newly generated css file is loaded on the next request.
    if (!file_exists($css_path)) {
      drupal_flush_all_caches();
    }

    $this->fileSystem->saveData($css_code, $css_path, FileSystemInterface::EXISTS_REPLACE);
    $this->fileSystem->chmod($css_path);

    // Flush asset caches.
    $this->cssCollectionOptimizerLazy->deleteAll();
    $this->jsCollectionOptimizerLazy->deleteAll();
    Cache::invalidateTags(['library_info']);

    // Change the dummy query string appended to CSS and JavaScript
    // files. This forces all browsers to reload fresh files.
    _drupal_flush_css_js();

    return new Response(JSON::encode('Success'), 200);
  }

  /**
   * Get live css from file.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An HTTP response.
   */
  public function get(Request $request): Response {
    $css_file_path = InjectCssFileUtility::FILE_PATH;
    if ($request->query->get('admin')) {
      $css_file_path = InjectCssFileUtility::FILE_PATH_ADMIN;
    }

    if (!file_exists($css_file_path)) {
      return new Response('', 200);
    }

    $data = file_get_contents($css_file_path);
    return new Response($data, 200);
  }

}
