<?php

namespace Drupal\simple_live_css\Utility;

/**
 * Provides utility function for the live_css module.
 */
class InjectCssFileUtility {

  const FILE_PATH = 'public://simple-live-css-inject.css';
  const FILE_PATH_ADMIN = 'public://simple-live-css-inject-admin.css';

  /**
   * Return the relative inject css path.
   *
   * @param bool $admin
   *   If set to TRUE, the returned file path is for the 'admin' live css.
   *
   * @return string
   *   A relative url.
   */
  public static function getRelativePath(bool $admin = FALSE): string {
    if ($admin) {
      return \Drupal::service('file_url_generator')->generateString(static::FILE_PATH_ADMIN);
    }
    return \Drupal::service('file_url_generator')->generateString(static::FILE_PATH);
  }

}
