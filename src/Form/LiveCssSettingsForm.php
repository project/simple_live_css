<?php

namespace Drupal\simple_live_css\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Simple Live CSS settings config form.
 *
 * @package Drupal\simple_live_css\Form
 */
class LiveCssSettingsForm extends ConfigFormBase {

  const CONFIG_KEY = 'simple_live_css.settings';

  /**
   * The config instance.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $configInstance;

  /**
   * Constructs a LiveCssSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);

    $this->configInstance = $this->config(self::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId(): string {
    return str_replace('.', '_', self::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['enable_admin_live_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable admin live CSS'),
      '#description' => $this->t('If enabled, a separate live CSS editor will be available on pages using the admin theme.'),
      '#default_value' => $this->configInstance->get('enable_admin_live_css'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->configInstance->set('enable_admin_live_css', $form_state->getValue('enable_admin_live_css'));
    $this->configInstance->save();
  }

}
