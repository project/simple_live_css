<?php

/**
 * @file
 * This file is used to write hooks that are used in the module.
 */

use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Url;
use Drupal\simple_live_css\Utility\InjectCssFileUtility;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function simple_live_css_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.simple_live_css':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module allows developers to apply styling directly to a live site. CSS can be added within a page, with live preview. The changes can then be saved to a CSS file that will be added to every page. It is also possible to enable a separate editor for pages using the admin theme.') . '</p>';
      $output .= '<p>' . t('The primary usage purpose of the module is (temporarily) overriding existing CSS.') . '</p>';
      $output .= '<h3>' . t('Usage') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Opening the interface') . '</dt>';
      $output .= '<dd>' . t('When the "Edit live CSS" permission is enabled for the currently logged in user, a <em>"LIVE CSS"</em> button will appear on every non-admin page of the site. Click this button to open the interface.') . '</dd>';
      $output .= '<dt>' . t('Using the interface') . '</dt>';
      $output .= '<dd>' . t('When writing CSS, styling will be synced to the page automatically. This, however, does not mean that every change is automatically saved. To write all changes to file, use the <em>"save"</em> button. To discard changes, simply use the <em>"close"</em> button.') . '</dd>';
      $output .= '<dt>' . t('Admin live CSS') . '</dt>';
      $output .= '<dd>' . t('To enable live CSS on admin pages, the <em>"Enable admin live CSS"</em> setting has to be enabled on the <a href=":settings_link">settings page</a>.', [':settings_link' => Url::fromRoute('live_css.settings')->toString()]) . '</dd>';
      $output .= '<dd>' . t('When enabled, users with the <code>edit admin live css</code> permission will have a live CSS editor available on pages that use the admin theme.') . '</dd>';
      $output .= '<dd>' . t('This admin live CSS is completely segregated from the regular live CSS. Changes you make here will only be visible on admin pages.') . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_page_attachments().
 */
function simple_live_css_page_attachments(array &$page) {
  $current_user = \Drupal::currentUser();
  $admin_enabled = \Drupal::config('simple_live_css.settings')->get('enable_admin_live_css');

  if (\Drupal::service('theme.manager')->getActiveTheme()->getName() === \Drupal::config('system.theme')->get('admin')) {
    if ($admin_enabled) {
      $page['#attached']['library'][] = 'simple_live_css/simple_live_css_admin';
      if ($current_user->hasPermission('edit admin live css')) {
        $page['#attached']['library'][] = 'simple_live_css/simple_live_css_editor';
        $page['#attached']['drupalSettings']['simple_live_css']['admin'] = TRUE;
        $page['#attached']['drupalSettings']['simple_live_css']['css_file_path'] = InjectCssFileUtility::getRelativePath(TRUE);
      }
    }
  }
  else {
    $page['#attached']['library'][] = 'simple_live_css/simple_live_css';
    if ($current_user->hasPermission('edit live css')) {
      $page['#attached']['library'][] = 'simple_live_css/simple_live_css_editor';
      $page['#attached']['drupalSettings']['simple_live_css']['css_file_path'] = InjectCssFileUtility::getRelativePath();
    }
  }
}

/**
 * Implements hook_library_info_alter().
 */
function simple_live_css_library_info_alter(array &$libraries, $extension) {
  if ($extension === 'simple_live_css') {
    if (file_exists(InjectCssFileUtility::FILE_PATH)) {
      $relative_path = InjectCssFileUtility::getRelativePath();
      $libraries['simple_live_css']['css']['theme'][$relative_path] = [
        'preprocess' => FALSE,
      ];
    }
    if (file_exists(InjectCssFileUtility::FILE_PATH_ADMIN)) {
      $relative_path = InjectCssFileUtility::getRelativePath(TRUE);
      $libraries['simple_live_css_admin']['css']['theme'][$relative_path] = [
        'preprocess' => FALSE,
      ];
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function simple_live_css_preprocess_html(array &$variables) {
  $current_user = \Drupal::currentUser();
  if ($current_user->hasPermission('edit live css') || $current_user->hasPermission('edit admin live css')) {
    $variables['attributes']['class'][] = 'js--live-css';
  }
}

/**
 * Implements hook_css_alter().
 *
 * Make sure the custom inject file is loaded last,
 * since we want to be able to override any (theme) css.
 *
 * @todo Refactor once conditional ordering is introduced in https://www.drupal.org/node/1945262.
 */
function simple_live_css_css_alter(&$css, AttachedAssetsInterface $assets) {
  foreach ([InjectCssFileUtility::getRelativePath(), InjectCssFileUtility::getRelativePath(TRUE)] as $relative_path) {
    $path = ltrim($relative_path, '/');
    if (isset($css[$path])) {
      $css[$path]['group'] = 300;
    }
  }
}
