/**
 * @file
 * Js for the live css service.
 */

(function ($) {

  'use strict';

  /**
   * The live css service constructor.
   *
   * @constructor
   */
  let LiveCssService = function () {};

  LiveCssService.prototype.save = function (code, admin) {

    let deferred = $.Deferred();

    $.ajax({
      url: '/live_css/save',
      type: 'POST',
      dataType: 'json',
      data: {
        'css': code,
        'admin': admin,
      },
      success: function (data) {
        deferred.resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        deferred.reject(textStatus);
      }
    });

    return deferred.promise();
  };

  LiveCssService.prototype.getCss = function (admin) {

    let deferred = $.Deferred();

    $.ajax({
      url: '/live_css/get',
      type: 'GET',
      data: {
        admin: admin,
      },
      dataType: 'text',
      success: function (data) {
        deferred.resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        deferred.reject(textStatus);
      }
    });

    return deferred.promise();
  };

  window.LiveCssService = LiveCssService;

})(jQuery);
