/**
 * @file
 */

(function ($, Drupal, drupalSettings, once) {

  Drupal.behaviors.simple_live_css = {
    attach: function (context) {
      const isAdmin = drupalSettings.simple_live_css.admin;
      once('simple_live_css', '.js--live-css', context).forEach(function () {
        new LiveCssEditor(isAdmin);
      });
    }
  }

}(jQuery, Drupal, drupalSettings, once));
